const { Articles } = require('../models')
// const { nanoid } = require('nanoid')
const BaseController = require('./baseController')

class ArticleController extends BaseController {
    constructor() {
        super(Articles)
    }
}

module.exports = ArticleController
// function get(query) {
//     return Articles.findAll({
//         where: query
//     })
// }

// function add(data) {
//     return Articles.create({
//         id: nanoid(),
//         ...data
//     })
// }

// function edit(id, data) {
//     return Articles.update(data, {
//         where: { id }
//     })
// }

// function remove(id) {
//     return Articles.destroy({
//         where: { id }
//     })
// }

// module.exports = {
//     get,
//     add,
//     edit,
//     remove
// }

module.exports = ArticleController