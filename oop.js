// class itu objek tp propertynya dynemix dg kemampuan khusus( membuat dan enkapsulasi metod )
const obj = {
    firstName: "Abdur",
    lastName: "Rohim"
}
console.log(obj.firstName);

// gaya penulisan
class NamaClass {
    // constructuring adlh hal yang dibutuhkan u/ membangun class
    constructor(firstName, lastName, createHalfName) {
        // this adalah sebuah object yang berada didalam class
        this.firstName = firstName
        this.lastName = lastName
        if (createHalfName) {
            this.halfName = firstName.substring(0, 4)
        }
    }
    getFullName() {
        return `${this.firstName} ${this.lastName}`
    }
}

// new digunakan u/ memperanak 

//cara pakai class
const chania = new NamaClass("Chania", "Evangelista", true)
// cara menggunakan property *this* di class
console.log(chania.firstName);
console.log(chania.lastName);
console.log(chania.halfName);
console.log(chania.getFullName());

// inheritance
const bapak = {
    emas: true,
    sertifikatRumah: true
}

const anak = {
    ...bapak,
    hp: true
}
console.log(anak);

//inheritance pakai class
class Bapak {
    constructor(emas, sertifikatRumah) {
        this.emas = emas
        this.sertifikatRumah = sertifikatRumah
    }
}

const bapakClass = new Bapak(true, false)
console.log(bapakClass.emas);
console.log(bapakClass.sertifikatRumah);
console.log(bapakClass.hp);

class Anak extends Bapak {
    constructor(hp, emas, sertifikatRumah) {
        super(emas, sertifikatRumah)
        this.hp = hp
    }
}

const anakClass = new Anak(true, false, true)
console.log(anakClass.emas);
console.log(anakClass.sertifikatRumah);
console.log(anakClass.hp);
