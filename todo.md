## SET UP MVC
1. git init
2. npm init -y
3. npm install express pg dotenv nanoid sequelize ejs
4. buat file .env
5. buat .gitignore (node_modules, package-lock.json, .env)
6. sequelize init
7. config.json => config.js
   1. gunakan dotenv jadi environment variable
8. sequelize db:create
9. sequelize model:create --create Articles --attributes content:string
10. sequelize db:migrate
11. buat index.js
    1. isi inisiasi express
    2. ganti view engine ke ejs
12. buat folder route isi indexRoute.js
    1. respond dengan render index
13. buat folder views isi index.ejs
    1. isinya basic html structure dengan body h1 "Articles" dulu
14. buat script start & dev di package.json

## SETUP HEROKU
1. project sudah punya repository
2. sudah commit semua
3. heroku version
4. heroku login (in terminal)
5. heroku create app-name (in terminal)
   1. heroku create auth-api-habatus
   2. check git remote -v
6. heroku info (in terminal)
7. git remote add heroku
   
> untuk ngecek addon yang ada heroku
heroku addons:service
> untuk memfilter dengan yang punya nama postgres
heroku addons:service | grep postgres

> pasang addon ke server heroku kita
heroku addons:create heroku-postgresql

> ngecek addon apasaja yang keinstall diserver heroku
> yang pakai wsl tambah "-a app-name"
- heroku addons --all
> ngecek environment variable di server heroku 
- heroku config (wsl)
> tambah environment variable di server heroku
- heroku config: JWT_SECRET="" PGSSLMODE=no-verify 
> install sequelize-cli di dev dependency
- npm install -D sequelize-cli
> tambah script "build" dengan value `sequelize db:migrate`
> push aplikasi ke server (commit dulu ya)
- git push -f heroku master


